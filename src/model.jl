## basic model elements
mutable struct Affine
    W
    b
end
  
Affine(In::Int, Out::Int) = Affine(randn(Float32, Out, In), randn(Float32, Out))
  
function (m::Affine)(data)
    y = m.W * data.X .+ m.b
    return y
end
    
mutable struct AffineCRF
    W
    b
    w
    μ
end
  
AffineCRF(In::Integer, Out::Integer, Kernel::Integer) = AffineCRF(
    randn(Float32, Out, In), 
    randn(Float32, Out), 
    randn(Float32, Kernel),
    ones(Float32, Out, Out) - I
)
  
function (m::AffineCRF)(data)
    y = m.W * data.X .+ m.b
    h = deepcopy(y)
    for _ in 1:3   # three iterations are sufficient for a single CRF step
      h = Flux.softmax(h, dims=1)
      h2 = zero(h)
      for k in 1:length(data.f)
        h2 = h2 .+ h * data.f[k] .* view(m.w, :, k)
      end
      h = y .- m.μ * h2
    end
    return h
end

mutable struct Perceptron
    W1
    b1
    W2
    b2
end

Perceptron(In::Int, Hidden::Int, Out::Int) = Perceptron(
    randn(Float32, Hidden, In), 
    randn(Float32, Hidden), 
    randn(Float32, Out, Hidden), 
    randn(Float32, Out)
)

function (m::Perceptron)(data)
    y = m.W2 * relu.(m.W1 * data.X .+ m.b1) .+ m.b2
    return y
end


mutable struct PerceptronCRF
    W1
    b1
    W2
    b2
    w
    μ
end

PerceptronCRF(In::Integer, Hidden::Integer, Out::Integer, Kernel::Integer) = PerceptronCRF(
    randn(Float32, Hidden, In), 
    randn(Float32, Hidden), 
    randn(Float32, Out, Hidden), 
    randn(Float32, Out), 
    randn(Float32, Kernel),
    ones(Float32, Out, Out) - I
)

function (m::PerceptronCRF)(data)
    y = m.W2 * relu.(m.W1 * data.X .+ m.b1) .+ m.b2
    h = deepcopy(y)
    for _ in 1:3
        h = Flux.softmax(h, dims=1)
        h2 = zero(h)
        for k in 1:length(data.f)
            h2 = h2 .+ h * data.f[k] .* view(m.w, :, k)
        end
        h = y .- m.μ * h2
    end
    return h
end

Flux.@functor Affine (W, b, )
Flux.@functor AffineCRF (W, b, w, )
Flux.@functor Perceptron (W1, W2, b1, b2, )
Flux.@functor PerceptronCRF (W1, W2, b1, b2, w, )