##### Dice coefficient
dice(x, y, thx=0.5, thy=0.5) = 2count((x .> thx) .& (y .> thy)) / (count(x .> thx) + count(y .> thy))

##### Centroid 
"""
    centroid(data::AbstractArray{T}, coordinates::AbstractArray{<:Tuple}; thr::AbstractFloat=0.01f0) where T <: Real

Calculate the centroid of the given data array.

# Arguments
- `data`: A multidimensional array of type Real containing the probability (weights).
- `coordinates`: An array of tuples or CartesianIndex representing the coordinates of the data array.
- `thr`: (Optional) A threshold value. All elements of the data array less than this threshold will be set to zero. Default is 0.01.

# Returns
- Returns an array representing the centroid coordinates of the data array.

# Example
```julia
data = rand(5, 5)
coordinates = [(i, j) for i in 1:5, j in 1:5]
centroid(data, coordinates)
```
"""
function centroid(data::AbstractArray{T}, coordinates::AbstractArray{<:Tuple}; thr::AbstractFloat=0.01f0) where T <: Real
    size(data) == size(coordinates) || error("The dimensions of the array and its coordinates do not match. Please check.")
    data[data .< thr] .= 0
    #coors = collect.(zip(coordinates...))
	coors = [[x[k] for x in coordinates] for k in 1:length(first(coordinates))]
    return [sum(x .* data) ./ sum(data) for x in coors]
end

function centroid(data::AbstractArray{T}, coordinates::AbstractArray{<:CartesianIndex}; thr::AbstractFloat=0.01f0) where T <: Real
    centroid(data, map(Tuple, coordinates), thr=thr)
end

"""
    centroid_displacement(x::AbstractArray{T}, y::AbstractArray{T}, coordinates::Union{AbstractArray{<:Tuple}, AbstractArray{<:CartesianIndex}};
                          thrx::AbstractFloat=0f0, thry::AbstractFloat=0f0) where T <: Real

Calculate the displacement between the centroids of two data arrays.

# Arguments
- `x`: The first data array (weights) of type Real.
- `y`: The second data array (weights) of type Real.
- `coordinates`: An array of tuples or CartesianIndices representing the coordinates of the data arrays.
- `thrx`: (Optional) A threshold value for the first data array. Elements in `x` less than this threshold will be set to zero before calculating the centroid. Default is 0.
- `thry`: (Optional) A threshold value for the second data array. Elements in `y` less than this threshold will be set to zero before calculating the centroid. Default is 0.

# Returns
- Returns a single number representing the Euclidean distance between the centroids of the two data arrays.

# Example
```julia
x = rand(5, 5)
y = rand(5, 5)
coordinates = [(i, j) for i in 1:5, j in 1:5]
disp = centroid_displacement(x, y, coordinates)
```
"""
function centroid_displacement(
    x::AbstractArray{T}, y::AbstractArray{T}, coordinates::Union{AbstractArray{<:Tuple},AbstractArray{<:CartesianIndex}}; 
    thrx::AbstractFloat=0f0, thry::AbstractFloat=0f0
) where T <: Real
    return sqrt(sum((centroid(x, coordinates, thr=thrx) .- centroid(y, coordinates, thr=thry)) .^ 2))
end

##### a function to count connected components in the image
function count_blobs(data::AbstractArray{T}) where T <: Real
	adj = get_adj(data)
	#@info adj
	blobs = 0
	index = findall(x -> x > 0, data)
	if length(index) > 0
		visited = zeros(Int, length(index))
		for k in 1:length(index)
			if visit!(visited, adj, k) > 0
				blobs += 1
			end
		end	
	end
	return blobs
end

function visit!(visited::AbstractArray{Int}, adj::AbstractArray{Int}, k::Int)
	if (visited[k] == 0) && sum(visited .* adj[:, k]) .== 0
		visited[k] = 1
		for kk in findall(adj[:, k] .> 0)
			visit!(visited, adj, kk)
		end
		return 1
	elseif visited[k] == 0
		visited[k] = 1
		for kk in findall(adj[:, k] .> 0)
			visit!(visited, adj, kk)
		end
		return 0
	end
	return 0
end


"""
    save_to_nii(data; mask_name, output_name)

Save the given data to a NIfTI file format.

# Arguments
- `data`: The data array that needs to be saved in NIfTI format.
- `mask_name`: The name of the mask file to be used.
- `output_name`: The name of the output NIfTI file.

# Description
This function takes an array of data, a mask file, and an output file name. It saves
the data to a NIfTI file using the provided mask and names the output file with the given output name.

# Example
```julia
save_to_nii(my_data, mask_name="path_to_mask/mask.nii.gz", output_name="output.nii.gz")
```
"""
function save_to_nii(
	data::AbstractArray{Real};
	mask_name::String,
	output_name::String
)
	mask = niread(mask_name)
	index = findall(mask.raw > 0)
	newdata = zero(mask.raw)
	newdata[index] .= data
	vol = NIVolume(
		newdata, time_step=1, slice_duration=mask.header.slice_duration, regular=mask.header.regular,
        voxel_size=(mask.header.pixdim[2], mask.header.pixdim[3], mask.header.pixdim[4]), 
        orientation=vcat(collect(mask.header.srow_x)', collect(mask.header.srow_y)', collect(mask.header.srow_z)'),
	)
	niwrite(output_name, vol)
end

"""
    create_singleshell(input_fname::String, b::Real; output_dir::String, bvecs_fname::String, 
                       bvals_fname::String, n_directions::Int=32)

Create a single-shell diffusion MRI dataset by selecting gradients that are uniformly distributed on a sphere.

# Arguments
- `input_fname`: Path to the input diffusion MRI file (NIfTI format).
- `b`: The b-value to be used for the single-shell data.

# Keyword Arguments
- `output_dir`: The directory where the output files will be saved. Defaults to the directory of the input file.
- `bvecs_fname`: Path to the file containing gradient directions.
- `bvals_fname`: Path to the file containing the b-values.
- `n_directions`: Number of directions that will be uniformly distributed on a sphere. Defaults to 32.

# Output
The function will output a file named `data.nii.gz` containing the single-shell diffusion MRI data, and text files named
`bvecs` and `bvals` containing the gradient directions and b-values respectively.

# Errors
- Throws an error if the input file does not exist.
- Throws an error if the FSLDIR environment variable does not exist.
- Throws an error if bvals and bvecs do not match in size.
- Throws an error if bvecs is not in 3 x n_directions shape.

# Examples
```julia
create_singleshell("path/to/diffusion_data.nii.gz", 1000, output_dir="path/to/output", 
                   bvecs_fname="path/to/bvecs", bvals_fname="path/to/bvals", n_directions=32)
```				   
"""
function create_singleshell(
	input_fname::String, b::Real; 
	output_dir::String="$(dirname(abspath(input_fname)))",
	bvecs_fname::String="$(dirname(abspath(input_fname)))/bvecs", 
	bvals_fname::String="$(dirname(abspath(input_fname)))/bvals",
	n_directions::Int=32
)
	## error checkings
	isfile(input_fname) || error("$(input_fname) does not exist. ")
	haskey(ENV, "FSLDIR") || error("FSLDIR environment variable does not exist.")
	## load data and bvecs/bvals
	data = niread(input_fname)
	bvecs = readdlm(bvecs_fname, Float32)
	bvals = readdlm(bvals_fname, Float32)[:]
	length(bvals) == size(bvecs, 2) || error("bvals and bvecs do not match. ")
	size(bvecs, 1) == 3 || error("bvecs must be in 3 x n_directions shape.")
	## create n random directions uniformly on a sphere
	out_fname = "$(output_dir)/my_dirs"
	args=["$(ENV["FSLDIR"])/bin/gps", "--ndir=$(n_directions)", "--optws", "--out=$(out_fname)"]
	#run(`$(ENV["FSLDIR"])/bin/gps --ndir=$n_directions --optws --out=$out_fname`)
	run(Cmd(args))
	mydirs = readdlm("$(out_fname)$(n_directions).txt", Float32)
	b0idx = findall(x -> x < 100, bvals)
	b1idx = findall(x -> b-100 < x < b+100, bvals)
	_, maxind = findmax(abs.(mydirs * view(bvecs, :, b1idx)), dims=2)
	maxind = [x[2] for x in maxind]
	idx = sort(vcat(b0idx, b1idx[maxind])[:])
	newdata = view(data.raw, :, :, :, idx)
	vol = NIVolume(
        newdata, time_step=1, slice_duration=data.header.slice_duration, regular=data.header.regular,
        voxel_size=(data.header.pixdim[2], data.header.pixdim[3], data.header.pixdim[4]), 
        orientation=vcat(collect(data.header.srow_x)', collect(data.header.srow_y)', collect(data.header.srow_z)'),
    )
	niwrite(joinpath(output_dir, "data.nii.gz"), vol)
	open(joinpath(output_dir, "bvecs"), "w") do io
		writedlm(io, bvecs[:, idx])
	end
	open(joinpath(output_dir, "bvals"), "w") do io
		writedlm(io, bvals[idx])
	end
end

"""
    create_masks(; ref::String, warp::String, out::Union{String,Nothing}=nothing,
                  aparc::Union{String,Nothing}=nothing, brainmask::Union{String,Nothing}=nothing)

Create brain masks by warping a given reference image using a non-linear warp field.

# Keyword Arguments
- `ref`: Path to the reference image (e.g. in NIfTI format) that will be warped.
- `warp`: Path to the non-linear warp field used to warp the reference image.
- `out`: (Optional) Path to the output directory where the masks will be saved. If not provided, masks will be saved in the current directory.
- `aparc`: (Optional) Path to the aparc+aseg file. This is required for certain masks creation.
- `brainmask`: (Optional) Path to the binary brain mask in reference space.

# Output
This function will output brain masks by warping the reference image. The masks will be saved to the directory specified by the `out` argument or in the current directory if `out` is not provided.

# Errors
- Throws an error if the FSLDIR environment variable does not exist.
- Throws an error if the provided reference image or warp field file does not exist.

# Examples
```julia
create_masks(ref="path/to/reference_image.nii.gz", warp="path/to/warpfield.nii.gz", 
             out="path/to/output_dir", aparc="path/to/aparc+aseg.nii.gz")
```
"""
function create_masks(;
	ref::String,
	warp::String,
	out::Union{String,Nothing}=nothing,
	aparc::Union{String,Nothing}=nothing,
	brainmask::Union{String,Nothing}=nothing
)
	haskey(ENV, "FSLDIR") || error("FSLDIR environment variable does not exist.")
	isfile(ref) || error("$(ref) does not exist.")
	isfile(warp) || error("$(warp) does not exist.")
	cmd = joinpath(dirname(@__DIR__), "scripts", "create_masks.sh")
	args = [cmd, "--ref=$ref", "--warp=$warp"]
	params = [("out", out), ("aparc", aparc), ("brainmask", brainmask)]
	for (param, value) in params
    	if value !== nothing
        	push!(args, "--$param=$value")
    	end
	end
	run(Cmd(args))
end

"""
    create_tracts(; samples_dir::String, input_dir::String, seed::Union{String,Nothing}=nothing,
                   xfm::Union{String,Nothing}=nothing, ref::Union{String,Nothing}=nothing,
                   out::Union{String,Nothing}=nothing, brainmask::Union{String,Nothing}=nothing,
                   nsteps::Union{Real,Nothing}=nothing, cthr::Union{Real,Nothing}=nothing,
                   fibthresh::Union{Real,Nothing}=nothing, nsamples::Union{Real,Nothing}=nothing,
                   steplength::Union{Real,Nothing}=nothing, sampvox::Union{Real,Nothing}=nothing,
                   distthresh::Union{Real,Nothing}=nothing, gpu::Bool=true)

Create tracts for the specified seed using fiber samples estimated via BedpostX.

# Keyword Arguments
- `samples_dir`: Directory that stores fiber orientation files, e.g., data.bedpostX.
- `input_dir`: Input directory that stores anatomical masks in native T1 space.
- `seed`: (Optional) The seed mask, usually in the individual T1 native space.
- `xfm`: (Optional) The transformation matrix that maps the reference T1 space to the diffusion space.
- `ref`: (Optional) The reference image, usually in the individual T1 native space.
- `out`: (Optional) Output directory to store tract density maps.
- `brainmask`: (Optional) Binary brain mask in the reference space.
- `nsteps`: (Optional) Number of steps per sample.
- `cthr`: (Optional) Curvature threshold.
- `fibthresh`: (Optional) Volume fraction before subsidiary fiber orientations are considered.
- `nsamples`: (Optional) Number of samples.
- `steplength`: (Optional) Step length in mm.
- `sampvox`: (Optional) Sample random points within a sphere with radius x mm from the center of the seed voxels.
- `distthresh`: (Optional) Discards samples shorter than this threshold (in mm).
- `gpu`: (Optional) Use the GPU version of probtrackx2 if set to `true` (default), otherwise use the CPU version.

# Output
This function creates tracts and saves them to the output directory specified by the `out` argument.

# Errors
- Throws an error if the FSLDIR environment variable does not exist.
- Throws an error if the provided samples directory or input directory does not exist.

# Examples
```julia
create_tracts(samples_dir="path/to/data.bedpostX", input_dir="path/to/roi_folder",
              seed="path/to/seed.nii.gz", out="path/to/output_dir")
```
"""
function create_tracts(;
	samples_dir::String,
	input_dir::String,
	seed::Union{String,Nothing}=nothing,
	xfm::Union{String,Nothing}=nothing,
	ref::Union{String,Nothing}=nothing,
	out::Union{String,Nothing}=nothing,
	brainmask::Union{String,Nothing}=nothing,
	nsteps::Union{Real,Nothing}=nothing,
	cthr::Union{Real,Nothing}=nothing,
	fibthresh::Union{Real,Nothing}=nothing,
	nsamples::Union{Real,Nothing}=nothing,
	steplength::Union{Real,Nothing}=nothing,
	sampvox::Union{Real,Nothing}=nothing,
	distthresh::Union{Real,Nothing}=nothing,
	gpu::Bool=true,
)
	haskey(ENV, "FSLDIR") || error("FSLDIR environment variable does not exist.")
	isdir(samples_dir) || error("Directory $(samples_dir) does not exist.")
	isdir(input_dir) || error("Directory $(input_dir) does not exist.")

	cmd=joinpath(dirname(@__DIR__), "scripts", "create_tracts.sh")
	args = [cmd, "--samples=$samples_dir", "--inputdir=$input_dir"]
	params = [
		("out", out), 
		("seed", seed), 
		("xfm", xfm), 
		("ref", ref), 
		("brainmask", brainmask),
		("nsteps", nsteps),
		("cthr", cthr),
		("fibthresh", fibthresh),
		("nsamples", nsamples),
		("steplength", steplength),
		("sampvox", sampvox),
		("distthresh", distthresh)
	]
	for (param, value) in params
    	if value !== nothing
        	push!(args, "--$param=$value")
			#cmd = "$(cmd)" * " --$param=$value"
    	end
	end
	if gpu
		push!(args, "--gpu")
	end
	#run(`$(join(args, " "))`)
	run(Cmd(args))
end

"""
    connectivity_driven(; target1::String, target2::String, out::String, 
                        target3::Union{String,Nothing}=nothing,
                        thr1::Union{Real,Nothing}=nothing, thr2::Union{Real,Nothing}=nothing,
                        thr3::Union{Real,Nothing}=nothing, thr::Union{Real,Nothing}=nothing)

Perform a connectivity-driven analysis using two or three target images.

# Keyword Arguments
- `target1`: Path to the first target image file.
- `target2`: Path to the second target image file.
- `out`: Output directory path where the results should be stored.
- `target3`: (Optional) Path to the third target image file.
- `thr1`: (Optional) Threshold for the first target. Values below this threshold will be ignored.
- `thr2`: (Optional) Threshold for the second target. Values below this threshold will be ignored.
- `thr3`: (Optional) Threshold for the third target. Values below this threshold will be ignored if the third target is provided.
- `thr`: (Optional) Global threshold applied to all targets. If specified, this overrides `thr1`, `thr2`, and `thr3`.

# Output
This function performs a connectivity-driven analysis and saves the output in the directory specified by the `out` argument.

# Errors
- Throws an error if the FSLDIR environment variable does not exist.
- Throws an error if the provided target1 or target2 file paths do not exist.

# Example
```julia
connectivity_driven(target1="path/to/target1.nii.gz", target2="path/to/target2.nii.gz",
                    out="path/to/output_dir", thr1=0.5, thr2=0.6)
```
"""
function connectivity_driven(;
	target1::String,	
	target2::String,
	out::String,
	target3::Union{String,Nothing}=nothing,
	thr1::Union{Real,Nothing}=nothing,
	thr2::Union{Real,Nothing}=nothing,
	thr3::Union{Real,Nothing}=nothing,
	thr::Union{Real,Nothing}=nothing
)	
	haskey(ENV, "FSLDIR") || error("FSLDIR environment variable does not exist.")
	isfile(target1) || error("$target1 does not exist.")
	isfile(target2) || error("$target2 does not exist.")
	cmd=joinpath(dirname(@__DIR__), "scripts", "connectivity_driven.sh")
	args = [cmd, "--target1=$target1", "--target2=$target2", "--out=$out"]
	params = [
		("thr1", thr1),
		("thr2", thr2),
		("thr3", thr3),
		("target3", target3),
		("thr", thr)
	]
	for (param, value) in params
    	if value !== nothing
        	push!(args, "--$param=$value")
			#cmd = "$(cmd)" * " --$param=$value"
    	end
	end
	run(Cmd(args))
end