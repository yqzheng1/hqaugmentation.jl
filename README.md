# HQAugmentation.jl: A Image Quality Transfer Toolbox for Localising DBS Targets in Low-quality Data

HQAugmentation.jl is a julia toolbox developed to address the challenges associated with accurately targeting DBS targets on low-quality clinical-like dataset. This toolbox uses Image Quality Transfer techniques to transfer anatomical information from high-quality data to a wide range of connectivity features in low-quality data. The goal is to augment the inference on DBS targets localisation even with compromised data quality. We also have a Python implementation [here](https://git.fmrib.ox.ac.uk/yqzheng1/python-localise) (ongoing). For more details, please check our paper [here](https://link.springer.com/chapter/10.1007/978-3-031-43996-4_17)

## Setup

To start using this tool, you can add this tool to your working environment by:

```julia
using Pkg
Pkg.add(path="/path/to/your/HQAugmentation.jl")
# Import the module
using HQAugmentation
```

## Usage

### 1. **Preparation step**: generate connectivity features using tractography (either on high- or low-quality data)

If you haven't done so, you may want to create anatomical masks and run tractography to generate connectivity features. If you have your own tract density maps, please skip to [2. Train a HQAugmentation Model](#2-train-a-hqaugmentation-model).

#### 1.1 Create anatomical masks

To create anatomical masks, you can either run the shell scripts under `/scripts` by

```bash
/path/to/HQAugmentation.jl/scripts/create_masks.sh --ref=/path/to/t1.nii.gz --warp=/path/to/warp.nii.gz --out=/path/to/roi --aparc=/path/to/aparc.a2009s+aseg.nii.gz
```

where `--ref=t1.nii.gz` and `--warp=warp.nii.gz` are required arguments, specifying the reference image and the warp field that maps MNI 1mm space to the reference space. It is recommended to have your own cortical parcellation file `--aparc=aparc.a2009s+aseg.nii.gz`, otherwise standard ROIs will be employed. If the output directory is unspecified, it will create a folder called `roi` under the same directory as the reference image. More instructions can be found by typing `/path/to/HQAugmentation.jl/scripts/create_masks.sh --help`.

You can also run this command in julia, after importing this tool:

```julia
create_masks(ref="/path/to/t1.nii.gz", warp="/path/to/warp.nii.gz", aparc="/path/to/aparc.a2009s+aseg.nii.gz", out="/path/to/roi")
```

#### 1.2 Create tract density maps (connectivity features)

To create tract density maps, you can either run the following in command line

```bash
/path/to/HQAugmentation.jl/scripts/create_tract.sh --samples=/path/to/data.bedpostX --inputdir=/path/to/roi/left --seed=/path/to/seed.nii.gz --out=/path/to/my_output_dir
```

where `--samples` and `--inputdir` are required arguments. You must specify the folder containing `merged_*samples.nii.gz` samples, e.g., the output directory from FSL's BedpostX. `--inputdir` is the directory that stores anatomical masks. If you don't specify the seed, it will use `tha.nii.gz` under the `inputdir`. More info can be found by typing `/path/to/HQAugmentation.jl/scripts/create_tract.sh --help`.

You can also run this in julia by executing the following function:

```julia
create_tracts(
    samples_dir="/path/to/mydata.bedpostX", 
    input_dir="/path/to/roi/left", 
    seed="/path/to/seed.nii.gz",
    out="/path/to/my_output_dir"
)
```

#### 1.3 Create connectivity-driven Vim

If you have high-quality data, you may create "ground truth" location using the following connectivity-driven approach.

```shell
/path/to/HQAugmentation.jl/scripts/connectivity_driven.sh --target1=seeds_to_m1.nii.gz --thr1=50 --target2=seeds_to_cerebellum.nii.gz --thr2=15 --out=label.nii.gz
```

will create connectivity-driven Vim. You can do so in Julia as well

```julia
connectivity_driven(
    target1="seeds_to_m1.nii.gz", 
    thr1=50,
    target2="seeds_to_cerebellum.nii.gz",
    thr2=15,
    output="output.nii.gz"
)
```

will find the maximum connectivity region with respect to target1, and target2. You can specify the corresponding thresholds to remove voxels characterised by low tract-density, otherwise no thresholds will be applied to these tract density maps.

### 2. Train a HQAugmentation Model

First, you need to load the data. Suppose your data is organised in this way:

```bash
subject001/
├── streamlines
│   ├── left
│       ├── seeds_to_target1.nii.gz
│       ├── seeds_to_target2.nii.gz
│   ├── right
│       ├── seeds_to_target1.nii.gz
│       ├── seeds_to_target2.nii.gz
├── roi
│   ├── left
│       ├── tha.nii.gz
│       ├── atlas.nii.gz
│   ├── right
│       ├── tha.nii.gz
│       ├── atlas.nii.gz
├── high-quality-labels
│   ├── left
│       ├── labels.nii.gz
│   ├── right
│       ├── labels.nii.gz
└── otherfiles
subject002/
subject003/
```

You can load the data by running the following (e.g., left hemisphere):

```julia
## load data for a single subject as a single batch, left hemisphere
batch = load_data(
    "subject001"; 
    mask_name="roi/left/tha.nii.gz",
    target_path="streamlines/left", 
    target_list=["seeds_to_target1.nii.gz", "seeds_to_target2.nii.gz"],
    label_name="high-quality-labels/left/labels.nii.gz"
)

## if you want to include the group-average (atlas) as an additional feature
## you must specify a group-average file 
## e.g., subject001/roi/left/atlas.nii.gz is the group-average probability map 
## warped into subject001's native space
batch = load_data(
    "subject001"; 
    mask_name="roi/left/tha.nii.gz",
    target_path="streamlines/left", 
    target_list=["seeds_to_target1.nii.gz", "seeds_to_target2.nii.gz"],
    atlas="roi/left/atlas.nii.gz", withgroup=true,
    label_name="high-quality-labels/left/labels.nii.gz"
)

## load connectivity features of multiple subjects
## If the file structure is the same for all subjects, you can simply feed in an Array of subject IDs
training_set = load_data(
    ["subject001", "subject002", "subject003"]; 
    mask_name="roi/left/tha.nii.gz",
    target_path="streamlines/left", 
    target_list=["seeds_to_target1.nii.gz", "seeds_to_target2.nii.gz"],
    label_name="high-quality-labels/left/labels.nii.gz"
)
test_set = load_data(
    ["subject004", "subject005", "subject006"]; 
    mask_name="roi/left/tha.nii.gz",
    target_path="streamlines/left", 
    target_list=["seeds_to_target1.nii.gz", "seeds_to_target2.nii.gz"],
    label_name="high-quality-labels/left/labels.nii.gz"
)
```

Once you have loaded your training data, you can start training the model now:

```julia
using Flux
## Train a HQAugmentation model
model = train(training_set, test_set)
## Specify number of epochs
model = train(training_set, test_set, n_epochs=50)
```

### 2. Making predictions on low-quality data

```julia
## After you have trained a model, you can then make predictions for a new subject, e.g. lowq_subject
X = load_features(
    "lowq_subject"; 
    mask_name="roi/left/tha.nii.gz",
    target_path="streamlines/left", 
    target_list=["seeds_to_target1.nii.gz", "seeds_to_target2.nii.gz"],
)
## predicted_map is the final prediction.
predicted_map = model(X)

## if you want to save it to a nii file my_predicted_map.nii.gz
save_to_nii(predicted_map, mask_name="lowq_subject/roi/left/tha.nii.gz", output_name="lowq_subject/my_predicted_map.nii.gz")
```

## License

This project is licensed under the MIT License - see the [LICENSE](./LICENSE) for details.

## Contributing

We greatly appreciate contributions from the community! If you're interested in improving HQAugmentation.jl, there are many ways you can contribute.

### Reporting Issues

If you're experiencing a problem with HQAugmentation.jl, please open an issue on the GitHub repository. When submitting an issue, try to include as much detail as you can. This should include the exact steps to reproduce the issue, your operating system, and version of HQAugmentation.jl.

### Fixing bugs or adding new features

If you're interested in contributing code to fix open issues or adding new features, please follow these steps:

 1. Fork the repository and clone it locally.
 2. Create a branch for your edits.
 3. Add, commit, and push your changes.
 4. Submit a pull request.

When submitting a pull request, please make sure to include a descriptive title and clear description of your changes.

### Documentation

If you spot a problem with the documentation or think it could be clearer, you can make amendments by editing the relevant files and submitting a pull request.

Remember, contributions aren't just about code - any way in which you can help us improve is much appreciated!

Thank you for your interest in contributing to HQAugmentation.jl!
