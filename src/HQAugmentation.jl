module HQAugmentation

using Flux: train!
using LinearAlgebra
using Parameters
using Flux
using SparseArrays
using NIfTI
using DelimitedFiles
using Statistics
using JLD2

include("utils.jl")
include("load.jl")
include("model.jl")

export Affine, AffineCRF, Perceptron, PerceptronCRF
export my_train!, train_loop!, train
export load_data, load_features, load_labels
export dice, centroid, centroid_displacement
export create_masks, create_tracts, create_singleshell, connectivity_driven, save_to_nii

## gradient descent function
function my_train!(loss, ps, data, opt)
    ps = Flux.Params(ps)
    for k in eachindex(data)
      gs = gradient(ps) do
        loss(data[k]...)
      end
      Flux.Optimise.update!(opt, ps, gs)
    end
end

### L1 and L2 loss
l2(x) = sum(x.^2)
l1(x) = sum(abs.(x))

"""
    train_loop!(model::Union{Affine,AffineCRF}, training_data::AbstractArray, test_data::AbstractArray;
                loss_func=Flux.Losses.logitcrossentropy, opt=ADAM(0.01), λ1=1f-4, λ2=1f-4,
                n_epochs=20, timeout=20) -> model

    train_loop!(model::Union{Perceptron,PerceptronCRF}, training_data::AbstractArray, test_data::AbstractArray;
                loss_func=Flux.Losses.logitcrossentropy, opt=ADAM(0.01), λ1=1f-4, λ2=1f-4,
                n_epochs=20, timeout=20) -> model

Train a given neural network model using provided training data and evaluate the model on test data.

# Arguments
- `model`: The model to be trained. This can be an instance of Affine, AffineCRF, Perceptron or PerceptronCRF.
- `training_data`: A vector of tuples (features_batch, labels_batch) representing the training dataset.
- `test_data`: A vector of tuples (features_batch, labels_batch) representing the test dataset.
- `loss_func`: (Optional) The loss function to be used for training. Default is Flux.Losses.logitcrossentropy.
- `opt`: (Optional) The optimization algorithm to be used for training. Default is ADAM with a learning rate of 0.01.
- `λ1`: (Optional) Regularization parameter for L1 regularization. Default is 1e-4.
- `λ2`: (Optional) Regularization parameter for L2 regularization. Default is 1e-4.
- `n_epochs`: (Optional) Number of epochs for which the model will be trained. Default is 20.
- `timeout`: (Optional) The maximum number of seconds to wait before re-evaluating the loss on the test data. Default is 20.

# Returns
- Returns the trained model.

# Example
```julia
trained_model = train_loop!(model, training_data, test_data, loss_func=Flux.Losses.logitcrossentropy, 
                            opt=ADAM(0.01), λ1=1e-4, λ2=1e-4, n_epochs=20, timeout=20)
```
"""
function train_loop!(
    model::Union{Affine,AffineCRF}, 
    training_data::AbstractArray, 
    test_data::AbstractArray; 
    loss_func=Flux.Losses.logitcrossentropy, 
    opt=ADAM(0.01), 
    λ1=1f-4, 
    λ2=1f-4, 
    n_epochs=20,
    timeout=20
)
    λ1 = convert(Float32, λ1)
    λ2 = convert(Float32, λ2)
    loss(x, y) = loss_func(model(x), y) + λ2 * sum(l2, Flux.params(model.W)) + λ1 * sum(l1, Flux.params(model.W))
    evalcb() = @show(mean([loss(test_x, test_y) for (test_x, test_y) in test_data]))
    throttled_cb = Flux.throttle(evalcb, timeout)
    for epoch in 1:n_epochs
        Flux.train!(loss, Flux.params(model), training_data, opt, cb = throttled_cb)
    end
    return model
end

function train_loop!(
    model::Union{PerceptronCRF,Perceptron}, 
    training_data::AbstractArray, 
    test_data::AbstractArray; 
    loss_func=Flux.Losses.logitcrossentropy, 
    opt=ADAM(0.01), 
    λ1=1f-4, 
    λ2=1f-4, 
    n_epochs=20,
    timeout=20
)
    λ1 = convert(Float32, λ1)
    λ2 = convert(Float32, λ2)
    loss(x, y) = loss_func(model(x), y) + λ2 * sum(l2, Flux.params(model.W1, model.W2)) + λ1 * sum(l1, Flux.params(model.W1, model.W2))
    evalcb() = @show(mean([loss(test_x, test_y) for (test_x, test_y) in test_data]))
    throttled_cb = Flux.throttle(evalcb, timeout)
    @inbounds for epoch in 1:n_epochs
        Flux.train!(loss, Flux.params(model), training_data, opt, cb=throttled_cb)
    end
    return model
end

"""
    train(training_data, test_data; model=:Perceptron, loss_func=Flux.Losses.logitcrossentropy,
          opt=ADAM(0.01), λ1=1f-4, λ2=1f-4, n_epochs=20, timeout=20) -> trained_model

Train a neural network model using the provided training data and evaluate the model using test data. 
The function supports training either an Affine model or a Perceptron model with a given set of hyperparameters.

# Arguments
- `training_data`: Training dataset. It should be a vector where each element is a tuple representing 
                  the input features and target labels.
- `test_data`: Test dataset. Similar to training_data, it should be a vector containing the test features
               and target labels.
- `model`: (Optional) A Symbol specifying the type of model to be trained. It can be either `:Affine` or `:Perceptron`. 
           Default is `:Perceptron`.
- `loss_func`: (Optional) Loss function to be used for training. Default is `Flux.Losses.logitcrossentropy`.
- `opt`: (Optional) Optimization algorithm to be used for training. Default is `ADAM` with a learning rate of `0.01`.
- `λ1`: (Optional) Regularization parameter for L1 regularization. Default is `1e-4`.
- `λ2`: (Optional) Regularization parameter for L2 regularization. Default is `1e-4`.
- `n_epochs`: (Optional) Number of epochs for which the model will be trained. Default is `20`.
- `timeout`: (Optional) The maximum number of seconds to wait before re-evaluating the loss on the test data. 
             Default is `20`.

# Returns
- Returns the trained neural network model (`trained_model`).

# Example
```julia
trained_model = train(training_data, test_data, model=:Affine, loss_func=Flux.Losses.logitcrossentropy,
                      opt=ADAM(0.01), λ1=1e-4, λ2=1e-4, n_epochs=20, timeout=20)
```
"""
function train(
    training_data, test_data;
    model=:Perceptron,
    loss_func=Flux.Losses.logitcrossentropy, 
    opt=ADAM(0.01), 
    λ1=1f-4, 
    λ2=1f-4, 
    n_epochs=20,
    timeout=20
)
    n_features = size(training_data[1][1].X, 1)
    if model == :Affine
        m_init = Affine(n_features, 2)
        print("Start initialising....\n")
        train_loop!(
            m_init, 
            training_data, 
            test_data;
            loss_func=loss_func,
            opt=opt,
            λ1=λ1,
            λ2=λ2,
            n_epochs=n_epochs,
            timeout=timeout
        )
        m = AffineCRF(deepcopy(m_init.W), deepcopy(m_init.b), [0.5f0, ], ones(Float32, 2, 2) - I)
    elseif model == :Perceptron
        m_init = Perceptron(n_features, 2, 2)
        print("Start initialising....\n")
        train_loop!(
            m_init, 
            training_data, 
            test_data;
            loss_func=loss_func,
            opt=opt,
            λ1=λ1,
            λ2=λ2,
            n_epochs=n_epochs,
            timeout=timeout
        )
        m = PerceptronCRF(
            deepcopy(m_init.W1), deepcopy(m_init.b1), 
            deepcopy(m_init.W2), deepcopy(m_init.b2), 
            [0.5f0, ], 
            ones(Float32, 2, 2) - I
        )
    else
        error("Please specify the model. either :Perceptron or :Affine. ")
    end
    print("Initialising done. Start training....\n")
    train_loop!(
        m, 
        training_data, 
        test_data;
        loss_func=loss_func,
        opt=opt,
        λ1=λ1,
        λ2=λ2,
        n_epochs=n_epochs,
        timeout=timeout
    )
    print("Training done.")
    return m
end
end # module
