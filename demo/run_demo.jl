using Pkg
Pkg.add(path="https://git.fmrib.ox.ac.uk/yqzheng1/hqaugmentation.jl.git")

using HQaugmentation

cd("/path/to/HQAugmentation.jl/test_data")
train_list = ["100307", "100408", "100610"]
val_list = ["101006", "101107", "101309"]
# load training data
# the streamline features have been saved in
train_data = [
    load_data(
        subject, 
        mask_name="roi/left/tha_small.nii.gz",
        label_name="high-quality-labels/left/labels.nii.gz", 
        atlas="roi/left/atlas.nii.gz", 
        withgroup=true, 
        data="streamlines/left/features_single32.jld2", 
        power=[2f0, 1f0, 0.5f0, 0.2f0]
    ) for subject in train_list
]

# load test data
val_data = [
    load_data(
        subject, 
        mask_name="roi/left/tha_small.nii.gz",
        label_name="high-quality-labels/left/labels.nii.gz", 
        atlas="roi/left/atlas.nii.gz", 
        withgroup=true, 
        data="streamlines/left/features_single32.jld2", 
        power=[2f0, 1f0, 0.5f0, 0.2f0]
    ) for subject in val_list

]

# train a model
model = train(train_data, val_data, n_epochs=500, timeout=1, λ1=1f-2, λ2=1f-2)

# making predictions
test_data = load_features(
    "101915", 
    mask_name="roi/left/tha_small.nii.gz",
    atlas="roi/left/atlas.nii.gz", 
    withgroup=true, 
    data="streamlines/left/features_single32.jld2", 
    power=[2f0, 1f0, 0.5f0, 0.2f0]
)
predictions = model(test_data)

# save as nii using the same mask
save_to_nii(predictions, "101915/roi/left/tha_small.nii.gz", "101915/predictions.nii.gz")