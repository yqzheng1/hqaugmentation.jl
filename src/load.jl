function load_default_list()
    filename = joinpath(dirname(@__DIR__), "resources", "default_target_list.txt")
    return readlines(filename)
end

const DEFAULT_TARGET_LIST = load_default_list()

### a struct that defines neighbourhood system
struct Adj
	inds1::AbstractArray{Int}
	inds2::AbstractArray{Int}
	n::Int
end

### generate sparse adjacency matrix
function get_adj_sparse(mask::AbstractArray{T, 3}) where T <: Real
	# load mask and data
	#mask = niread(join([subject, "native", hemi, "mist_$(hemi)_thalamus_mask_1.25.nii.gz"], '/'))
	index = findall(x -> x!=0, mask)
	n = length(index)
	# 26 neighbours system
	neighbours = Array{CartesianIndex}(undef, 26)
	# find adj list
	#indices = Array{Array}(undef, n)
	inds = []
    #inds1, inds2 = ([] for _ = 1:2)
    @inbounds for v ∈ 1:n
		x, y, z = [index[v][k] for k ∈ 1:3]
		copyto!(neighbours, [
			# same y slice
			CartesianIndex(x+1, y, z+1), CartesianIndex(x-1, y, z-1), 
			CartesianIndex(x+1, y, z), CartesianIndex(x-1, y, z), 
			CartesianIndex(x, y, z+1), CartesianIndex(x, y, z-1), 
			CartesianIndex(x+1, y, z-1), CartesianIndex(x-1, y, z+1), 
			# y-1 slice
			CartesianIndex(x, y-1, z), 
			CartesianIndex(x+1, y-1, z+1), CartesianIndex(x-1, y-1, z-1),
			CartesianIndex(x+1, y-1, z), CartesianIndex(x-1, y-1, z),
			CartesianIndex(x, y-1, z+1), CartesianIndex(x, y-1, z-1),
			CartesianIndex(x+1, y-1, z-1), CartesianIndex(x-1, y-1, z+1), 
			# y+1 slice
			CartesianIndex(x, y+1, z), 
			CartesianIndex(x+1, y+1, z+1), CartesianIndex(x-1, y+1, z-1),
			CartesianIndex(x+1, y+1, z), CartesianIndex(x-1, y+1, z),
			CartesianIndex(x, y+1, z+1), CartesianIndex(x, y+1, z-1),
			CartesianIndex(x+1, y+1, z-1), CartesianIndex(x-1, y+1, z+1), 
		])
		push!(inds, [(v, kk) for (kk, x) in enumerate(index) if x ∈ neighbours])
		#push!(inds2, [kk for (kk, x) in enumerate(index) if x ∈ neighbours])
		#push!(inds1, [v for _ = 1:length(inds2[end])])
            #adj = sparse(, , n, n)
    end
	inds = vcat(inds...)
	inds1 = [el[1] for el in inds]
	inds2 = [el[2] for el in inds]
        #indices = vcat(indices...)
        #adj = sparse([el[1] for el in indices], [el[2] for el in indices], 1, n, n)
	return inds1, inds2, n
end

function get_adj_sparse_dict(mask::AbstractArray{T, 3}) where T <: Real
    index = findall(x -> x!=0, mask)
    n = length(index)
    index_map = OrderedDict(zip(index, 1:n))

    inds = []

    @inbounds for v in 1:n
        x, y, z = Tuple(index[v])
        neighbours = [
            CartesianIndex(x+i, y+j, z+k) 
            for i in -1:1, j in -1:1, k in -1:1 
            if !(i == 0 && j == 0 && k == 0)
        ]

        for neighbour in neighbours
            if haskey(index_map, neighbour)
                push!(inds, (v, index_map[neighbour]))
            end
        end
    end

    inds1 = [el[1] for el in inds]
    inds2 = [el[2] for el in inds]
    
    return inds1, inds2, n
end

"""
    CRFBatch

A mutable struct representing a (flattened) batch for the Conditional Random Field (CRF) model. The struct contains several 
parameters including the number of classes (`K`), the feature matrix (`X`), the adjacency structure (`adj`), 
the dimensions of the data (`n`, `d`), the length scale (`γ`), and the constructed kernel matrix (`f`).
"""
@with_kw mutable struct CRFBatch{T<:Real}
    K::Int = 2
    X::AbstractArray{T}
    adj::Adj
    n::Int = size(X, 2)
    d::Int = size(X, 1)
    γ::AbstractArray{T} = convert(Array{eltype(X)}, [0]) # length scale
    f::AbstractArray{AbstractArray} = construct_kernel(X, adj, γ)
end

"""
    construct_kernel(X::AbstractArray{T}, adj::Adj, γ::T)

Constructs a kernel matrix for the given feature matrix (`X`), adjacency structure (`adj`), and length scale (`γ`).
This function handles both scalar and array `γ` values, returning either a sparse matrix or a list of sparse matrices.
"""
function construct_kernel(X::AbstractArray{T}, adj::Adj, γ::T) where T<:Real
    if γ == 0
        return sparse(adj.inds2, adj.inds1, 1f0, adj.n, adj.n)
    else
        valid_inds = [k for k in 1:adj.n if adj.inds1[k] < adj.inds2[k]]
        adj_inds1 = adj.inds1[valid_inds]
        adj_inds2 = adj.inds2[valid_inds]
        vals = [exp.(-sum((view(X, :, k1) .- view(X, :, k2)) .^ 2) .* γ) for (k1, k2) in zip(adj_inds1, adj_inds2)]
        f = sparse(adj_inds1, adj_inds2, vals, adj.n, adj.n)
        return f .+ f'
    end
end

"""
    construct_kernel(X::AbstractArray{T}, adj::Adj, γ::AbstractArray{T})

An overload of the `construct_kernel` function that accepts an array of `γ` values. It constructs a kernel matrix
for each `γ` value and returns a list of these matrices.
"""
function construct_kernel(X::AbstractArray{T}, adj::Adj, γ::AbstractArray{T}) where T<:Real
    return [construct_kernel(X, adj, el) for el in γ]
end

"""
    load_features(subject::String; mask_name::String, target_path::Union{String,Nothing}=nothing,
                  data::Union{String,Nothing}=nothing, atlas::Union{String,Nothing}=nothing,
                  target_list::AbstractArray{String}=DEFAULT_TARGET_LIST, demean::Bool=true,
                  withgroup::Bool=false, normalise::Bool=true, γ::Array{Float32}=[0f0], 
                  power::Array{Float32}=[2f0, 1f0, 0.5f0], save_features::Bool=false,
                  output_fname::String=joinpath(target_path, "features.jld2")) -> CRFBatch

Load features for a given subject, process them, and return in a `CRFBatch` structure. Optionally, save the processed features to a file.

# Arguments
- `subject`: A string or vector of strings specifying the path to the subject directory.
- `mask_name`: The name of the mask file for the subject, relative to the subject directory.
- `target_path`: (Optional) Path to the tract-density data directory, relative to the subject directory. Default is `nothing`.
- `data`: (Optional) The name of the file that contains the pre-saved data matrix, relative to the subject directory. Default is `nothing`.
- `atlas`: (Optional) The name of the file containing the atlas in individual space, relative to the subject directory. Default is `nothing`.
- `target_list`: (Optional) List of target names to load. Default is `DEFAULT_TARGET_LIST`.
- `demean`: (Optional) Whether to demean the features. Default is `true`.
- `withgroup`: (Optional) Whether to load the group-average as an additional feature. Default is `false`.
- `normalise`: (Optional) Whether to normalize the maximum tract density to 1. Default is `true`.
- `γ`: (Optional) A Float32 array. Default is `[0f0]`.
- `power`: (Optional) A Float32 array specifying powers to which the data should be raised to expand the features. Default is `[2f0, 1f0, 0.5f0]`.
- `save_features`: (Optional) Whether to save the feature matrix for later analysis. Default is `false`.
- `output_fname`: (Optional) The name of the file to save the feature matrix, relative to the subject directory. Default is "features.jld2" in the `target_path`.

# Returns
- Returns a `CRFBatch` structure containing the processed features.

# Example
```julia
batch = load_features("path/to/subject", mask_name="mask.nii", target_path="path/to/targets", save_features=true)
```
"""
function load_features(
    subject::String;
    mask_name::String,
    target_path::Union{String,Nothing}=nothing,
    data::Union{String,Nothing}=nothing,
    atlas::Union{String,Nothing}=nothing,
    target_list::AbstractArray{String}=DEFAULT_TARGET_LIST,
    demean::Bool=true, withgroup::Bool=false, 
    normalise::Bool=true,
    γ::Array{Float32}=[0f0], 
    power::Array{Float32}=[2f0, 1f0, 0.5f0],
    save_features::Bool=false,
    output_fname::Union{String,Nothing}=nothing
)
    if withgroup && (atlas === nothing) 
        error("If withgroup is set to true, you must specify the file that contains the atlas in the individual space. ")
    end

    if (data === nothing) && (target_path === nothing)
        error("Please specify either target_path or data. ")
    end

    if (save_features) && (output_fname === nothing)
        error("Please specify the output filename that stores the feature matrix. ")
    end

    # load mask
    mask = niread(joinpath(subject, mask_name)).raw
    index = findall(x -> x > 0, mask)
    # generate adjacency matrix
    inds1, inds2, n = get_adj_sparse(mask)
    n_targets = length(target_list)

    # load data into X
    if data === nothing
        X = zeros(Float32, n, n_targets)
        @inbounds for k = 1:n_targets
            X[:, k] .= convert(Array{Float32}, niread(joinpath(subject, target_path, target_list[k])).raw[index])
        end

        # if you want to save the feature matrix for later analysis...
        if save_features
            jldsave(joinpath(subject, output_fname); X=X)
        end
    else
        ## load pre-saved data
        X = load(joinpath(subject, data), "X")
        (size(X, 1) == n) || 
        error(
            "Dimension of the mask and the loaded data matrix do not match. ", 
            "Please check if the loaded data used the same mask."
        )
    end

    if withgroup
        ## load group-average as an additional feature
        ygroup = niread(joinpath(subject, atlas)).raw[index]
        ygroup[ygroup .< 0.01] .= 0
        ygroup ./= maximum(ygroup)
        X = hcat([X .^ el for el in power]..., ygroup)
    else
        X = hcat([X .^ el for el in power]...)
    end

    # maximum tract density normalised to 1
    if normalise
        X ./= maximum(X, dims=1)
    end
    X[isnan.(X)] .= 0; X[isinf.(X)] .= 0
    if demean
        X .-= mean(X, dims=1)
    end
    return CRFBatch(X=convert(Matrix{Float32}, X'), adj=Adj(inds1, inds2, n), γ=γ)
    #return CRFBatch(X=convert(Matrix{Float32}, X), adj=Adj(inds1, inds2, n), γ=γ, ), convert(Array{Float32}, vcat(1 .- y', y'))
end

function load_features(
    subjects::AbstractArray{String};
    mask_name::String,
    target_path::Union{String,Nothing}=nothing,
    data::Union{String,Nothing}=nothing,
    atlas::Union{String,Nothing}=nothing,
    target_list::AbstractArray{String}=DEFAULT_TARGET_LIST,
    demean::Bool=true, withgroup::Bool=false, 
    normalise::Bool=true,
    γ::Array{Float32}=[0f0], 
    power::Array{Float32}=[2f0, 1f0, 0.5f0],
    save_features::Bool=false,
    output_fname::Union{String,Nothing}=nothing
)
    return [
        load_features(
            subject,
            mask_name=mask_name,
            target_path=target_path,
            data=data,
            atlas=atlas,
            target_list=target_list,
            demean=demean,
            withgroup=withgroup,
            normalise=normalise,
            γ=γ,
            power=power,
            save_features=save_features,
            output_fname=output_fname
        ) for subject in subjects
    ]
end

"""
    load_labels(subject::String; mask_name::String, label_name::String) -> Array

Load labels for a given subject by applying a mask and returns them in a concatenated format.

# Arguments
- `subject`: A string or vector of strings specifying the path to the subject directory.
- `mask_name`: The name of the mask file for the subject, relative to the subject directory.
- `label_name`: The name of the label file for the subject, relative to the subject directory.

# Returns
- Returns an array with the labels in a one-hot format.

# Example
```julia
labels = load_labels("path/to/subject", mask_name="mask.nii.gz", label_name="labels.nii.gz")
```
"""
function load_labels(
    subject::String; 
    mask_name::String,
    label_name::String
)
    mask = niread(joinpath(subject, mask_name)).raw
    index = findall(x -> x > 0, mask)
    y = [x > 0 ? 1 : 0 for x in niread(joinpath(subject, label_name)).raw[index]]
    return vcat(1 .- y', y')
end

function load_labels(
    subjects::AbstractArray{String};
    mask_name::String,
    label_name::String,
)
    return [load_labels(subject, mask_name=mask_name, label_name=label_name) for subject in subjects]
end

"""
    load_data(subject::String; label_name::String, mask_name::String, target_path::Union{String,Nothing}=nothing,
              data::Union{String,Nothing}=nothing, atlas::Union{String,Nothing}=nothing,
              target_list::AbstractArray{String}=DEFAULT_TARGET_LIST, demean::Bool=true, 
              withgroup::Bool=false, normalise::Bool=true, γ::Array{Float32}=[0f0], 
              power::Array{Float32}=[2f0, 1f0, 0.5f0], save_features::Bool=false,
              output_fname::String=joinpath(target_path, "features.jld2")) -> Tuple

Load and process feature data and labels for a given subject.

# Arguments
- `subject`: A string or vector of strings specifying the path to the subject directory.
- `label_name`: The name of the label file for the subject, relative to the subject directory.
- `mask_name`: The name of the mask file for the subject, relative to the subject directory.
- `target_path`: (Optional) A string specifying the target directory path, relative to the subject directory. Default is `nothing`.
- `data`: (Optional) A string specifying pre-saved data file path, relative to the subject directory. Default is `nothing`.
- `atlas`: (Optional) A string specifying the atlas file path in the individual space, relative to the subject directory. Default is `nothing`.
- `target_list`: (Optional) An array of strings specifying the target list. Default is `DEFAULT_TARGET_LIST`.
- `demean`: (Optional) A boolean indicating whether to demean the features. Default is `true`.
- `withgroup`: (Optional) A boolean indicating whether to use group-average as an additional feature. Default is `false`.
- `normalise`: (Optional) A boolean indicating whether to normalize the features. Default is `true`.
- `γ`: (Optional) An array of Float32 specifying the values of γ. Default is `[0f0]`.
- `power`: (Optional) An array of Float32 specifying the power values to be applied. Default is `[2f0, 1f0, 0.5f0]`.
- `save_features`: (Optional) A boolean indicating whether to save the feature matrix for later analysis. Default is `false`.
- `output_fname`: (Optional) A string specifying the output file name for saving features, relative to the subject directory. Default is `"features.jld2"` in `target_path`.

# Returns
- Returns a tuple where the first element is the features processed (CRFBatch) based on the given arguments and the second element is an array with the labels in one-hot format.

# Example
```julia
features, labels = load_data("path/to/subject", label_name="labels.nii", mask_name="mask.nii", target_path="path/to/target")
```
"""
function load_data(
    subject::String;
    label_name::String,
    mask_name::String,
    target_path::Union{String,Nothing}=nothing,
    data::Union{String,Nothing}=nothing,
    atlas::Union{String,Nothing}=nothing,
    target_list::AbstractArray{String}=DEFAULT_TARGET_LIST,
    demean::Bool=true, withgroup::Bool=false, 
    normalise::Bool=true,
    γ::Array{Float32}=[0f0], 
    power::Array{Float32}=[2f0, 1f0, 0.5f0],
    save_features::Bool=false,
    output_fname::Union{String,Nothing}=nothing
)
    return load_features(
        subject,
        mask_name=mask_name,
        data=data,
        target_path=target_path,
        atlas=atlas,
        target_list=target_list,
        demean=demean,
        withgroup=withgroup,
        normalise=normalise,
        γ=γ,
        power=power,
        save_features=save_features,
        output_fname=output_fname
    ), load_labels(
        subject,
        mask_name=mask_name, 
        label_name=label_name
    )
end

function load_data(
    subjects::AbstractArray{String};
    label_name::String,
    mask_name::String,
    target_path::Union{String,Nothing}=nothing,
    data::Union{String,Nothing}=nothing,
    atlas::Union{String,Nothing}=nothing,
    target_list::AbstractArray{String}=DEFAULT_TARGET_LIST,
    demean::Bool=true, withgroup::Bool=false, 
    normalise::Bool=true,
    γ::Array{Float32}=[0f0], 
    power::Array{Float32}=[2f0, 1f0, 0.5f0],
    save_features::Bool=false,
    output_fname::Union{String,Nothing}=nothing
)
    return [load_data(
        subject,
        label_name=label_name,
        mask_name=mask_name,
        target_path=target_path,
        data=data,
        atlas=atlas,
        target_list=target_list,
        demean=demean,
        withgroup=withgroup,
        normalise=normalise,
        γ=γ,
        power=power,
        save_features=save_features,
        output_fname=output_fname,
    ) for subject in subjects]
end